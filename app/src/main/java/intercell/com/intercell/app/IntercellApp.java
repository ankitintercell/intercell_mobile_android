package intercell.com.intercell.app;

import android.app.Application;
import android.content.Context;

import intercell.com.intercell.di.component.ApplicationComponent;
import intercell.com.intercell.di.component.DaggerApplicationComponent;
import intercell.com.intercell.di.module.ApplicationModule;

/**
 * Created by Dell on 12/15/2017.
 */

public class IntercellApp extends Application {

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent= DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
        mApplicationComponent.inject(this);
    }

    public static IntercellApp get(Context context) {
        return (IntercellApp) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }


    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }

}
