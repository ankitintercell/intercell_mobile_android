package intercell.com.intercell.di.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by Dell on 12/15/2017.
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
