package intercell.com.intercell.di.module;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import intercell.com.intercell.di.annotation.ApplicationContext;

/**
 * Created by Dell on 12/15/2017.
 */

@Module
public class ApplicationModule {

    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }
}
