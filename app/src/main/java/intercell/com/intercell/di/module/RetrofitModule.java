package intercell.com.intercell.di.module;

import javax.inject.Named;


import dagger.Module;
import dagger.Provides;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.utils.AppConstants;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dell on 12/21/2017.
 */
@Module
public class RetrofitModule {
    private static final String NAME_BASE_URL = "NAME_BASE_URL";

    @Provides
    @Named(NAME_BASE_URL)
    String provideBaseUrlString(){
        return AppConstants.BaseUrl;

    }

    @Provides

    Converter.Factory provideGsonConverter(){
        return GsonConverterFactory.create();

    }


    @Provides
    Retrofit provideRetrofit(Converter.Factory converter, @Named(NAME_BASE_URL) String baseUrl){
        return  new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(converter).build();
    }

    @Provides
    ApiInterface provideUsdaApi(Retrofit retrofit){
        return retrofit.create(ApiInterface.class);
    }

}
