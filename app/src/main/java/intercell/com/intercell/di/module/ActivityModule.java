package intercell.com.intercell.di.module;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import intercell.com.intercell.di.annotation.ActivityContext;
import intercell.com.intercell.di.annotation.PerActivity;
import intercell.com.intercell.ui.login.LoginMvpPresenter;
import intercell.com.intercell.ui.login.LoginMvpView;
import intercell.com.intercell.ui.login.LoginPresenter;
import intercell.com.intercell.ui.splash.SplashMvpPresenter;

import intercell.com.intercell.ui.splash.SplashPresenter;

/**
 * Created by Dell on 12/18/2017.
 */

@Module
public class ActivityModule {


    public AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    @ActivityContext
    Context providesContext() {
        return mActivity;
    }

//    @PerActivity
//    @Provides
//    SplashMvpPresenter provideSplashPresenter(
//            SplashPresenter presenter) {
//        return presenter;
//    }
//
//
//    @PerActivity
//    @Provides
//    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
//            LoginPresenter<LoginMvpView> presenter) {
//        return presenter;
//    }



}
