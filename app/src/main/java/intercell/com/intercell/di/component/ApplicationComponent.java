package intercell.com.intercell.di.component;

import android.app.Application;
import android.content.Context;

import dagger.Component;
import intercell.com.intercell.app.IntercellApp;
import intercell.com.intercell.di.annotation.ApplicationContext;
import intercell.com.intercell.di.module.ApplicationModule;
import intercell.com.intercell.di.module.RetrofitModule;

/**
 * Created by Dell on 12/15/2017.
 */
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(IntercellApp myApplication);

    @ApplicationContext
    Context context();

    Application application();


}
