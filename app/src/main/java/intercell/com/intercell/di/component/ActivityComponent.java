package intercell.com.intercell.di.component;

import dagger.Component;
import intercell.com.intercell.di.annotation.PerActivity;
import intercell.com.intercell.di.module.ActivityModule;

import intercell.com.intercell.di.module.RetrofitModule;
import intercell.com.intercell.ui.category.CategoryFragment;
import intercell.com.intercell.ui.confirm_order.ConfirmOrderFragment;
import intercell.com.intercell.ui.forgotpassword.ForgotActivity;
import intercell.com.intercell.ui.home.HomeFragment;
import intercell.com.intercell.ui.login.LoginActivity;
import intercell.com.intercell.ui.main.MainActivity;
import intercell.com.intercell.ui.mentordeatil.MentorDetailFragment;
import intercell.com.intercell.ui.mentorlist.MentorListFragment;
import intercell.com.intercell.ui.otp.OtpActivity;
import intercell.com.intercell.ui.resetPassword.ResetPasswordActivity;
import intercell.com.intercell.ui.selectdate.SelectDateFragment;
import intercell.com.intercell.ui.selecttime.SelectTimeFragment;
import intercell.com.intercell.ui.signup.SignupActivity;
import intercell.com.intercell.ui.splash.SplashActivity;
import intercell.com.intercell.ui.subcategories.SubCategoryFragment;
import intercell.com.intercell.ui.tc.TcActivity;

/**
 * Created by Dell on 12/18/2017.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, RetrofitModule.class})
public interface ActivityComponent {


    void inject(SplashActivity activity);

    void inject(LoginActivity loginActivity);

    void inject(SignupActivity signupActivity);

    void  inject(ForgotActivity forgotActivity);

    void inject(TcActivity tcActivity);

    void inject(OtpActivity otpActivity);

    void  inject(ResetPasswordActivity resetPasswordActivity);

    void  inject(MainActivity mainActivity);

    void inject(HomeFragment homeFragment);

    void inject(CategoryFragment categoryFragment);

    void inject(SubCategoryFragment subCategoryFragment);

    void inject(MentorListFragment mentorListFragment);

    void inject(MentorDetailFragment mentorDetailFragment);

    void inject(SelectDateFragment selectDateFragment);

    void inject(SelectTimeFragment selectTimeFragment);

    void inject(ConfirmOrderFragment confirmOrderFragment);
}
