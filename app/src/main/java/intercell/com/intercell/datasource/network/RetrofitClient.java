package intercell.com.intercell.datasource.network;

import dagger.Provides;
import intercell.com.intercell.utils.AppConstants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dell on 12/20/2017.
 */

public class RetrofitClient {


    private  static Retrofit retrofit= null;



    public  static Retrofit getClient(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BaseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }
        return  retrofit;
    }

}
