package intercell.com.intercell.datasource.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 1/12/2018.
 */

public class TcData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("short_code")
    @Expose
    private String shortCode;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("cms_desc")
    @Expose
    private String cmsDesc;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCmsDesc() {
        return cmsDesc;
    }

    public void setCmsDesc(String cmsDesc) {
        this.cmsDesc = cmsDesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}