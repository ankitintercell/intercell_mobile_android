package intercell.com.intercell.datasource.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 1/12/2018.
 */

public class TcResponse {

    @SerializedName("data")
    @Expose
    private TcData data;

    public TcData getData() {
        return data;
    }

    public void setData(TcData data) {
        this.data = data;
    }
}
