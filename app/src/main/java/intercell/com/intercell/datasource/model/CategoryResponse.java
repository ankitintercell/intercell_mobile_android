package intercell.com.intercell.datasource.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 1/23/2018.
 */

public class CategoryResponse {


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("detail")
    @Expose
    private String detail;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("stream_banner_text")
    @Expose
    private String streamBannerText;
    @SerializedName("stream_banner_image")
    @Expose
    private String streamBannerImage;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getStreamBannerText() {
        return streamBannerText;
    }

    public void setStreamBannerText(String streamBannerText) {
        this.streamBannerText = streamBannerText;
    }

    public String getStreamBannerImage() {
        return streamBannerImage;
    }

    public void setStreamBannerImage(String streamBannerImage) {
        this.streamBannerImage = streamBannerImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
