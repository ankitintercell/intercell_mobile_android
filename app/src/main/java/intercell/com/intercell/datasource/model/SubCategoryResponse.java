package intercell.com.intercell.datasource.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dell on 1/25/2018.
 */

public class SubCategoryResponse {

    @SerializedName("services")
    @Expose
    private List<SubCategories> services = null;


    public List<SubCategories> getServices() {
        return services;
    }

    public void setServices(List<SubCategories> services) {
        this.services = services;
    }


}
