package intercell.com.intercell.datasource.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dell on 1/30/2018.
 */

public class MentorDetailResponse implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("sub_category")
    @Expose
    private List<SubCategories> subCategory = null;
    @SerializedName("language")
    @Expose
    private List<MentorLanguage> language = null;
    @SerializedName("project")
    @Expose
    private Object project;
    @SerializedName("operation")
    @Expose
    private Object operation;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("highesteducation")
    @Expose
    private String highesteducation;
    @SerializedName("totalexperience")
    @Expose
    private String totalexperience;
    @SerializedName("brand_worked_for")
    @Expose
    private String brandWorkedFor;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("dp_image")
    @Expose
    private String dpImage;
    @SerializedName("category_name")
    @Expose
    private String categoryName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<SubCategories> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(List<SubCategories> subCategory) {
        this.subCategory = subCategory;
    }

    public List<MentorLanguage> getLanguage() {
        return language;
    }

    public void setLanguage(List<MentorLanguage> language) {
        this.language = language;
    }

    public Object getProject() {
        return project;
    }

    public void setProject(Object project) {
        this.project = project;
    }

    public Object getOperation() {
        return operation;
    }

    public void setOperation(Object operation) {
        this.operation = operation;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getHighesteducation() {
        return highesteducation;
    }

    public void setHighesteducation(String highesteducation) {
        this.highesteducation = highesteducation;
    }

    public String getTotalexperience() {
        return totalexperience;
    }

    public void setTotalexperience(String totalexperience) {
        this.totalexperience = totalexperience;
    }

    public String getBrandWorkedFor() {
        return brandWorkedFor;
    }

    public void setBrandWorkedFor(String brandWorkedFor) {
        this.brandWorkedFor = brandWorkedFor;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDpImage() {
        return dpImage;
    }

    public void setDpImage(String dpImage) {
        this.dpImage = dpImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
