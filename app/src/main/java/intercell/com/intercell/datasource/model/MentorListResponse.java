package intercell.com.intercell.datasource.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Dell on 1/25/2018.
 */

public class MentorListResponse implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("profile_summary")
    @Expose
    private Object profileSummary;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private Object mobile;
    @SerializedName("alternate_number")
    @Expose
    private Object alternateNumber;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("dp_image")
    @Expose
    private Object dpImage;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("pincode")
    @Expose
    private Object pincode;
    @SerializedName("fb")
    @Expose
    private Object fb;
    @SerializedName("ln")
    @Expose
    private Object ln;
    @SerializedName("gp")
    @Expose
    private Object gp;
    @SerializedName("tw")
    @Expose
    private Object tw;
    @SerializedName("user_ida")
    @Expose
    private String userIda;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("highesteducation")
    @Expose
    private String highesteducation;
    @SerializedName("totalexperience")
    @Expose
    private String totalexperience;
    @SerializedName("brand_worked_for")
    @Expose
    private String brandWorkedFor;
    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("category_name")
    @Expose
    private  String category_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Object getProfileSummary() {
        return profileSummary;
    }

    public void setProfileSummary(Object profileSummary) {
        this.profileSummary = profileSummary;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getMobile() {
        return mobile;
    }

    public void setMobile(Object mobile) {
        this.mobile = mobile;
    }

    public Object getAlternateNumber() {
        return alternateNumber;
    }

    public void setAlternateNumber(Object alternateNumber) {
        this.alternateNumber = alternateNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Object getDpImage() {
        return dpImage;
    }

    public void setDpImage(Object dpImage) {
        this.dpImage = dpImage;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Object getPincode() {
        return pincode;
    }

    public void setPincode(Object pincode) {
        this.pincode = pincode;
    }

    public Object getFb() {
        return fb;
    }

    public void setFb(Object fb) {
        this.fb = fb;
    }

    public Object getLn() {
        return ln;
    }

    public void setLn(Object ln) {
        this.ln = ln;
    }

    public Object getGp() {
        return gp;
    }

    public void setGp(Object gp) {
        this.gp = gp;
    }

    public Object getTw() {
        return tw;
    }

    public void setTw(Object tw) {
        this.tw = tw;
    }

    public String getUserIda() {
        return userIda;
    }

    public void setUserIda(String userIda) {
        this.userIda = userIda;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getHighesteducation() {
        return highesteducation;
    }

    public void setHighesteducation(String highesteducation) {
        this.highesteducation = highesteducation;
    }

    public String getTotalexperience() {
        return totalexperience;
    }

    public void setTotalexperience(String totalexperience) {
        this.totalexperience = totalexperience;
    }

    public String getBrandWorkedFor() {
        return brandWorkedFor;
    }

    public void setBrandWorkedFor(String brandWorkedFor) {
        this.brandWorkedFor = brandWorkedFor;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}
