package intercell.com.intercell.datasource.network;

import org.json.JSONObject;

import java.util.Map;

import intercell.com.intercell.datasource.model.BaseResponse;
import intercell.com.intercell.datasource.model.CategoryResponse;
import intercell.com.intercell.datasource.model.ForgotResponse;
import intercell.com.intercell.datasource.model.LoginResponse;
import intercell.com.intercell.datasource.model.MentorDetailResponse;
import intercell.com.intercell.datasource.model.MentorListResponse;
import intercell.com.intercell.datasource.model.SelectTimeResponse;
import intercell.com.intercell.datasource.model.SignupResponse;
import intercell.com.intercell.datasource.model.SubCategoryResponse;
import intercell.com.intercell.datasource.model.TcResponse;
import intercell.com.intercell.utils.AppConstants;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Dell on 12/20/2017.
 */

public interface ApiInterface {

    @Headers("Content-Type: application/json")
    @POST("core/Users/user_login")
    Call<LoginResponse> getUser(@Body Map<String, String> params);


    @Headers("Content-Type: application/json")
    @POST("core/Users/cms_page")
    Call<BaseResponse<TcResponse>> getTc(@Body Map<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("core/Users/signup_user")
    Call<BaseResponse<SignupResponse>> getRegister(@Body Map<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("core/Users/reset_password")
    Call<BaseResponse<ForgotResponse>> getFogotassword(@Body Map<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("core/Users/reset")
    Call<BaseResponse<SignupResponse>> setNewPassword(@Body Map<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("core/Users/send_phone_code")
    Call<BaseResponse<SignupResponse>> getOtp(@Body Map<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("core/Users/validate_key")
    Call<BaseResponse<SignupResponse>> getConfirmOtp(@Body Map<String, String> params);


    @Headers("Content-Type: application/json")
    @GET("core/LandingPage/send_services")
    Call<BaseResponse<CategoryResponse>> getCategoryList();


    @Headers("Content-Type: application/json")
    @GET("core/services/subservices-durations/{selected_id}")
    Call<BaseResponse<SubCategoryResponse>> getSubCategoryList(@Path(value = "selected_id", encoded = true) String userId);


    @Headers("Content-Type: application/json")
    @POST("core/services/counsellors-by-subservices")
    Call<BaseResponse<MentorListResponse>> getMentorList(@Body Map<String, String> params);


    @Headers("Content-Type: application/json")
    @GET("counsellor/Details/details/{userId_token}")
    Call<BaseResponse<MentorDetailResponse>> getMentorDeatil(@Path(value = "userId_token", encoded = true) String userId_token);


    @Headers("Content-Type: application/json")
    @GET("counsellor/calendars/lists/{counsellorId_startDate_endDate_token}")
    Call<BaseResponse<SelectTimeResponse>> getAvailTime(@Path(value = "counsellorId_startDate_endDate_token", encoded = true) String userId_token);
}
