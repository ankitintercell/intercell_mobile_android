package intercell.com.intercell.ui.mentorlist;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.MentorListResponse;
import intercell.com.intercell.ui.base.BaseActivity;
import intercell.com.intercell.ui.category.CategoryFragment;
import intercell.com.intercell.ui.mentordeatil.MentorDetailFragment;
import intercell.com.intercell.ui.selectdate.SelectDateFragment;

/**
 * Created by Dell on 1/25/2018.
 */

public class MentorListAdapter extends RecyclerView.Adapter<MentorListAdapter.MyViewHolder> {


    private List<MentorListResponse> categoryList;

    BaseActivity activity;

    public MentorListAdapter(List<MentorListResponse> categoryList, BaseActivity activity) {
        this.categoryList = categoryList;
        this.activity = activity;
    }

    @Override
    public MentorListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mentor_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MentorListAdapter.MyViewHolder holder, int position) {
        final MentorListResponse mentorListResponse = categoryList.get(position);
        holder.txt_name.setText(mentorListResponse.getFirstName() + " " + mentorListResponse.getLastName());
        holder.txt_brand.setText(mentorListResponse.getBrandWorkedFor());
        holder.txt_experience.setText(mentorListResponse.getTotalexperience());
        holder.txt_field.setText(mentorListResponse.getCategory());

        holder.btn_book_session.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SelectDateFragment mentorDetailFragment= new SelectDateFragment();
                onSwitchNext(mentorListResponse, SelectDateFragment.TAG,mentorDetailFragment);

            }
        });

        holder.btn_view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MentorDetailFragment mentorDetailFragment= new MentorDetailFragment();
                onSwitchNext(mentorListResponse, MentorDetailFragment.TAG,mentorDetailFragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img_mentor;

        public TextView txt_name, txt_experience, txt_field, txt_brand;

        public Button btn_view_profile, btn_book_session;


        public MyViewHolder(View view) {
            super(view);
            txt_name = (TextView) view.findViewById(R.id.txt_name);
            txt_experience = (TextView) view.findViewById(R.id.txt_experience);
            txt_field = (TextView) view.findViewById(R.id.txt_field);
            txt_brand = (TextView) view.findViewById(R.id.txt_brand);
            img_mentor = (ImageView) view.findViewById(R.id.img_mentor);
            btn_view_profile = (Button) view.findViewById(R.id.btn_view_profile);
            btn_book_session = (Button) view.findViewById(R.id.btn_book_session);


        }
    }

    public void onSwitchNext(MentorListResponse mentorListResponse, String TAG, Fragment fragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        SelectDateFragment fragment = new SelectDateFragment();
        Bundle bun = new Bundle();
        bun.putSerializable("selectedMentor", (Serializable) mentorListResponse);

        fragment.setArguments(bun);
        fragmentTransaction.add(R.id.containerView, fragment, TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
