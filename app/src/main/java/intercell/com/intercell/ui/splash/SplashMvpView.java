package intercell.com.intercell.ui.splash;

import intercell.com.intercell.ui.base.MvpView;

/**
 * Created by Dell on 12/20/2017.
 */

public interface SplashMvpView extends MvpView{
    void openLoginActivity();
}
