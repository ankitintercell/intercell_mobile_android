package intercell.com.intercell.ui.splash;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import intercell.com.intercell.R;
import intercell.com.intercell.ui.base.BaseActivity;
import intercell.com.intercell.ui.login.LoginActivity;

/**
 * Created by Dell on 12/21/2017.
 */

public class SplashActivity extends BaseActivity implements SplashMvpView {

    @Inject
    SplashPresenter<SplashMvpView> mvpViewSplashMvpPresenter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            getActivityComponent().inject(this);
            setContentView(R.layout.splash);
            mvpViewSplashMvpPresenter.onAttach(this);
            mvpViewSplashMvpPresenter.swithScreen();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void openLoginActivity() {
        Intent intent = LoginActivity.getStartIntent(SplashActivity.this);
        startActivity(intent);
        finish();
    }
}
