package intercell.com.intercell.ui.confirm_order;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import intercell.com.intercell.R;
import intercell.com.intercell.di.component.ActivityComponent;
import intercell.com.intercell.ui.base.BaseFragment;

/**
 * Created by Dell on 1/31/2018.
 */

public class ConfirmOrderFragment extends BaseFragment implements ConfirmOrderMvpView {

    protected View mView;


    @Inject
    ConfirmOrderPresener    <ConfirmOrderMvpView> confirmOrderMvpViewConfirmOrderMvpPresenter;

    TextView txt_total,txt_gst,txt_gst_rate,txt_sub_total,txt_time,txt_date
            ,txt_mentoring_cost,txt_minute;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.confirm_order, container, false);
        this.mView=view;
        ActivityComponent activityComponent= getActivityComponent();
        if(activityComponent!= null){
            setUp(mView);
            activityComponent.inject(this);
            confirmOrderMvpViewConfirmOrderMvpPresenter.onAttach(this);
        }

        return mView;
    }

    @Override
    public String getDeviceId() {
        return null;
    }

    @Override
    protected void setUp(View view) {
        txt_total=(TextView) view.findViewById(R.id.txt_total);
        txt_gst=(TextView) view.findViewById(R.id.txt_gst);
        txt_gst_rate=(TextView) view.findViewById(R.id.txt_gst_rate);
        txt_sub_total=(TextView ) view.findViewById(R.id.txt_sub_total);
        txt_time=(TextView) view.findViewById(R.id.txt_time);
        txt_date=(TextView) view.findViewById(R.id.txt_date);
        txt_mentoring_cost=(TextView) view.findViewById(R.id.txt_mentoring_cost);
        txt_minute=(TextView) view.findViewById(R.id.txt_minute);


    }
}
