package intercell.com.intercell.ui.otp;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import intercell.com.intercell.datasource.model.BaseResponse;
import intercell.com.intercell.datasource.model.SignupResponse;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.ui.base.BasePresenter;
import intercell.com.intercell.utils.AppConstants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 1/15/2018.
 */

public class OtpPresenter<V extends OtpMvpView> extends BasePresenter<V> implements OtpMvpPresenter<V> {

    @Inject
    ApiInterface apiInterface;

    @Inject
    public OtpPresenter() {

    }


    @Override
    public void onServerOtpClick(String phone, String token) {

        getMvpView().showLoading();
        try {


            //Here the json data is add to a hash map with key data
            Map<String, String> params = new HashMap<String, String>();
            params.put("phone", phone);
            params.put("token", token);

            params.put("os", AppConstants.osType);
            params.put("device_id", getMvpView().getDeviceId());

            apiInterface.getOtp(params).enqueue(new Callback<BaseResponse<SignupResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<SignupResponse>> call, Response<BaseResponse<SignupResponse>> response) {
                    getMvpView().hideLoading();

                    if (response.body().getStatus() != 1) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        getMvpView().onError(response.body().getMessage());


                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<SignupResponse>> call, Throwable t) {
                    getMvpView().hideLoading();

                }
            });
        } catch (Exception e) {

        }
    }

    @Override
    public void onconfirmOtpClick(String userId, String key) {

        getMvpView().showLoading();
        try {


            //Here the json data is add to a hash map with key data
            Map<String, String> params = new HashMap<String, String>();
            params.put("userId", userId);
            params.put("key", key.replace("\r\n",""));
            params.put("os", AppConstants.osType);
            params.put("device_id", getMvpView().getDeviceId());

            apiInterface.getConfirmOtp(params).enqueue(new Callback<BaseResponse<SignupResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<SignupResponse>> call, Response<BaseResponse<SignupResponse>> response) {
                    getMvpView().hideLoading();

                    if (response.body().getStatus() != 1) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        getMvpView().onError(response.body().getMessage());
                        getMvpView().openMainScrceen();


                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<SignupResponse>> call, Throwable t) {
                    getMvpView().hideLoading();

                }
            });
        } catch (Exception e) {

        }
    }
}
