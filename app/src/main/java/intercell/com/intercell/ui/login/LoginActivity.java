package intercell.com.intercell.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import intercell.com.intercell.R;
import intercell.com.intercell.ui.base.BaseActivity;
import intercell.com.intercell.ui.forgotpassword.ForgotActivity;
import intercell.com.intercell.ui.main.MainActivity;
import intercell.com.intercell.ui.signup.SignupActivity;
import intercell.com.intercell.ui.splash.SplashActivity;

/**
 * Created by Dell on 1/3/2018.
 */

public class LoginActivity extends BaseActivity implements LoginMvpView {


    @BindView(R.id.et_email)
    EditText mEmailEditText;

    @BindView(R.id.et_password)
    EditText mPasswordEditText;

    @BindView(R.id.bt_register)
    Button bt_register;

    @BindView(R.id.bt_forgot_password)
    Button bt_forgot_password;


    @Inject
    LoginPresenter<LoginMvpView> mvpViewLoginPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.login);
        mvpViewLoginPresenter.onAttach(this);
        ButterKnife.bind(this);

    }

    @Override
    public void openMainActivity() {

            Intent in = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(in);
            finish();


    }

    @OnClick(R.id.btn_server_login)
    void onServerLoginClick(View v) {
        mvpViewLoginPresenter.onServerLoginClick(mEmailEditText.getText().toString(),
                mPasswordEditText.getText().toString());
    }


    @OnClick(R.id.bt_register)
    void onRegisterClick(View v) {
        Intent intent = SignupActivity.getStartIntent(LoginActivity.this);
        startActivity(intent);

    }

    @OnClick(R.id.bt_forgot_password)
    void onForgotClick(View v) {
        Intent intent = ForgotActivity.getStartIntent(LoginActivity.this);
        startActivity(intent);

    }
}
