package intercell.com.intercell.ui.otp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.SignupResponse;
import intercell.com.intercell.ui.base.BaseActivity;
import intercell.com.intercell.ui.login.LoginActivity;
import intercell.com.intercell.ui.main.MainActivity;
import intercell.com.intercell.ui.resetPassword.ResetPasswordActivity;
import intercell.com.intercell.utils.AppConstants;

/**
 * Created by Dell on 1/15/2018.
 */

public class OtpActivity extends BaseActivity implements OtpMvpView {

    @BindView(R.id.et_otp)
    EditText et_otp;

    @BindView(R.id.btn_confirm)
    Button btn_confirm;


    @BindView(R.id.btn_resend)
    Button btn_resend;
    SignupResponse signupResponse;
    String lastScreen = "", userId = "",insertId="",mobile="",token="";
    @Inject
    OtpPresenter<OtpMvpView> otpMvpViewOtpPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.otp);

        otpMvpViewOtpPresenter.onAttach(this);
        ButterKnife.bind(this);
        AppConstants.checkAndRequestPermissions(this);
        lastScreen = getIntent().getStringExtra("lastScreen");
        if (lastScreen.equals("Signup")) {
            signupResponse = (SignupResponse) getIntent().getSerializableExtra("singupResponse");
            otpMvpViewOtpPresenter.onServerOtpClick(signupResponse.getMobile(), signupResponse.getToken());
        }
        else {
            userId = getIntent().getStringExtra("userId");
            insertId= getIntent().getStringExtra("insertId");
            mobile= getIntent().getStringExtra("mobile");
            token= getIntent().getStringExtra("token");
        }




    }

    @OnClick(R.id.btn_confirm)
    public void onConfirmClick(View v) {
        if(lastScreen.equals("Signup"))
            otpMvpViewOtpPresenter.onconfirmOtpClick(signupResponse.getUserId(), et_otp.getText().toString());
        else
            otpMvpViewOtpPresenter.onconfirmOtpClick(userId, et_otp.getText().toString());
    }

    @OnClick(R.id.btn_resend)
    public void onResendClick(View v) {
        if(lastScreen.equals("Signup")) {
            otpMvpViewOtpPresenter.onServerOtpClick(signupResponse.getMobile(), signupResponse.getToken());
        }else{
            otpMvpViewOtpPresenter.onServerOtpClick(mobile,"");
        }

    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).
                registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                String message = intent.getStringExtra("message");
                if (message.contains("Intercell")) {
                    message.replace(" ", "");
                    message = message.substring(message.length() - 8);
                    et_otp.setText(message);
                    if(lastScreen.equals("Signup")) {
                        otpMvpViewOtpPresenter.onconfirmOtpClick(signupResponse.getUserId(), message);
                    }else
                        otpMvpViewOtpPresenter.onconfirmOtpClick(userId, message);

                }


            }
        }
    };

    @Override
    public void openMainScrceen() {
        if(lastScreen.equals("forgotScreen")){
            Intent in = new Intent(OtpActivity.this, ResetPasswordActivity.class);
            in.putExtra("userId", userId);
            in.putExtra("insertId",insertId);
            in.putExtra("lastScreen", "otp");
            startActivity(in);
        }else{
            Intent in = new Intent(OtpActivity.this, MainActivity.class);
            startActivity(in);
            finish();

        }

    }
}
