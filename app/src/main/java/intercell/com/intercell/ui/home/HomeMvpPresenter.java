package intercell.com.intercell.ui.home;

import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/19/2018.
 */

public interface HomeMvpPresenter<V extends HomeMvpView> extends MvpPresenter<V> {
}
