package intercell.com.intercell.ui.forgotpassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.Serializable;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import intercell.com.intercell.R;
import intercell.com.intercell.ui.base.BaseActivity;
import intercell.com.intercell.ui.login.LoginActivity;
import intercell.com.intercell.ui.otp.OtpActivity;
import intercell.com.intercell.ui.signup.SignupActivity;

/**
 * Created by Dell on 1/10/2018.
 */

public class ForgotActivity extends BaseActivity implements ForgotMvpView {

    @Inject
    ForgotPresenter<ForgotMvpView> forgotMvpViewForgotPresenter;


    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.btn_confirm)
    Button btn_confirm;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, ForgotActivity.class);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.forgot_password);
        forgotMvpViewForgotPresenter.onAttach(this);
        ButterKnife.bind(this);


    }


    @OnClick(R.id.btn_confirm)
    public void onConfirmClick(View v) {

        forgotMvpViewForgotPresenter.onServerApiCall(et_email.getText().toString());
    }


    @Override
    public void openOTPActivity(String userId, String insertId, String mobile,String token) {
        Intent in = new Intent(ForgotActivity.this, OtpActivity.class);
        in.putExtra("userId", userId);
        in.putExtra("insertId", insertId);
        in.putExtra("mobile", mobile);
        in.putExtra("token",token);
        in.putExtra("lastScreen", "forgotScreen");
        startActivity(in);

    }
}
