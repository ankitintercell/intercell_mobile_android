package intercell.com.intercell.ui.selectdate;

import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/29/2018.
 */

public interface SelectDateMvpPresenter<V extends  SelectDateMvpView> extends MvpPresenter<V> {
}
