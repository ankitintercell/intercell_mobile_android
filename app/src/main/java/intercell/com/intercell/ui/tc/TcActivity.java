package intercell.com.intercell.ui.tc;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import intercell.com.intercell.R;
import intercell.com.intercell.ui.base.BaseActivity;
import intercell.com.intercell.ui.signup.SignupActivity;

/**
 * Created by Dell on 1/12/2018.
 */

public class TcActivity extends BaseActivity implements TcMvpView {
    @Inject
    TcPresenter<TcMvpView> tcMvpViewTcPresenter;

    @BindView(R.id.txt_description)
    TextView txt_description;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, TcActivity.class);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.term_condition);
        tcMvpViewTcPresenter.onAttach(this);
        tcMvpViewTcPresenter.onTcCall();
        ButterKnife.bind(this);
    }

    @Override
    public void showData(String description) {
        txt_description.setText(description);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
