package intercell.com.intercell.ui.mentorlist;

import java.util.List;

import intercell.com.intercell.datasource.model.SubCategories;
import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/25/2018.
 */

public interface MentorListMvpPresenter<V extends MentorListMvpView> extends MvpPresenter<V> {

    void onGetMentorList(String selectCategoryId, List<SubCategories> subCategoriesList,String token);



}
