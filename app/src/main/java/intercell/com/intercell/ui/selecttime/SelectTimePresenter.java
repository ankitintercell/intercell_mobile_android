package intercell.com.intercell.ui.selecttime;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import intercell.com.intercell.datasource.model.BaseResponse;
import intercell.com.intercell.datasource.model.SelectTimeResponse;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.ui.base.BasePresenter;
import intercell.com.intercell.utils.AppConstants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 1/31/2018.
 */

public class SelectTimePresenter<V extends SelectTimeMvpView> extends BasePresenter<V>
        implements SelectTimeMvpPresenter<V> {

    @Inject
    ApiInterface apiInterface;

    @Inject
    SelectTimePresenter() {

    }

    @Override
    public void getTimeList(String conusellorId, String startDate, String endDate, String token) {
        getMvpView().showLoading();


        try {


            apiInterface.getAvailTime(conusellorId + "/" + startDate + "/" + endDate + "/" +
                    token).enqueue(new Callback<BaseResponse<SelectTimeResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<SelectTimeResponse>> call,
                                       Response<BaseResponse<SelectTimeResponse>> response) {
                    getMvpView().hideLoading();
                    if (response.body().getStatus() == 0) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        getMvpView().onError(response.body().getMessage());
                        SelectTimeResponse selectTimeResponse = (SelectTimeResponse) response.body().getResponse().get(0);
                        getMvpView().bindTimeList(selectTimeResponse);

                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<SelectTimeResponse>> call, Throwable t) {
                    getMvpView().hideLoading();
                }
            });


        } catch (Exception e) {
            getMvpView().hideLoading();
            e.printStackTrace();
        }
    }
}
