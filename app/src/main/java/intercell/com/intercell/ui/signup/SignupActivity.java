package intercell.com.intercell.ui.signup;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.SignupResponse;
import intercell.com.intercell.ui.base.BaseActivity;
import intercell.com.intercell.ui.login.LoginActivity;
import intercell.com.intercell.ui.otp.OtpActivity;
import intercell.com.intercell.ui.tc.TcActivity;

/**
 * Created by Dell on 1/8/2018.
 */

public class SignupActivity extends BaseActivity implements SignupMvpView {


    private static int IMG_RESULT = 1;
    String ImageDecode;
    String[] FILE;
    int MY_PERMISSIONS_REQUEST_CAMERA = 123;
    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;


    @Inject
    SignupPresenter<SignupMvpView> signupMvpViewSignupPresenter;


    @BindView(R.id.et_last_name)
    EditText et_last_name;

    @BindView(R.id.et_first_name)
    EditText et_first_name;

    @BindView(R.id.et_gender)
    EditText et_gender;

    @BindView(R.id.et_role)
    EditText et_role;

    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.et_mobile)
    EditText et_mobile;


    @BindView(R.id.et_password)
    EditText et_password;

    @BindView(R.id.et_confirmPassword)
    EditText et_confirmPassword;

    @BindView(R.id.user_image)
    ImageView user_image;

    @BindView(R.id.txt_tc)
    TextView txt_tc;


    @BindView(R.id.sp_gender)
    Spinner sp_gender;

    @BindView(R.id.sp_role)
    Spinner sp_role;

    @BindView(R.id.btn_signup)
    Button btn_register;

    @BindView(R.id.chk_term)
    CheckBox chk_term;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, SignupActivity.class);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.signup);
        signupMvpViewSignupPresenter.onAttach(this);
        ButterKnife.bind(this);

    }


    @Override
    public void openOtpAcivity(SignupResponse signupResponse) {
        Intent in = new Intent(SignupActivity.this, OtpActivity.class);
        in.putExtra("singupResponse", (Serializable) signupResponse);
        in.putExtra("lastScreen", "Signup");
        startActivity(in);


    }


    @OnClick(R.id.btn_signup)
    public void onRegisterClick(View v) {
        signupMvpViewSignupPresenter.onServerSignupClick(et_first_name.getText().toString(),
                et_last_name.getText().toString(), sp_gender.getSelectedItem().toString(), sp_role.getSelectedItem().toString(),
                et_email.getText().toString(), et_mobile.getText().toString(), et_password.getText().toString(),
                et_confirmPassword.getText().toString(), chk_term.isChecked(), ImageDecode);
    }

    @OnClick(R.id.txt_tc)
    public void onTcClick(View v) {
        Intent intent = TcActivity.getStartIntent(SignupActivity.this);
        startActivity(intent);
    }

    @OnClick(R.id.user_image)
    public void onImageClick(View v) {

        if (Build.VERSION.SDK_INT < 23) {
            //Do not need to check the permission
            Intent intent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(intent, IMG_RESULT);
        } else {
            if (checkAndRequestPermissions()) {
                //If you have already permitted the permission
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(intent, IMG_RESULT);
            }
        }

    }


    private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);


        int storagePermission = ContextCompat.checkSelfPermission(this,


                Manifest.permission.WRITE_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
//        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
//            listPermissionsNeeded.add(Manifest.permission.CAMERA);
//        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,


                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCOUNTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //Permission Granted Successfully. Write working code here.
                    Intent intent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, IMG_RESULT);


                } else {
                    //You did not accept the request can not use the functionality.
                }
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (requestCode == IMG_RESULT && resultCode == RESULT_OK
                    && null != data) {


                Uri URI = data.getData();
                String[] FILE = {MediaStore.Images.Media.DATA};


                Cursor cursor = getContentResolver().query(URI,
                        FILE, null, null, null);

                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(FILE[0]);
                ImageDecode = cursor.getString(columnIndex);
                cursor.close();

                user_image.setImageBitmap(BitmapFactory
                        .decodeFile(ImageDecode));

            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
        }

    }
}
