package intercell.com.intercell.ui.signup;

import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/10/2018.
 */

public interface SignupMvpPresenter<V extends SignupMvpView> extends MvpPresenter<V> {

    void onServerSignupClick(String firstName, String lastName, String gender, String role,
                             String email, String mobile, String password,
                             String confirmPassword, boolean isTermChecked, String image);
}
