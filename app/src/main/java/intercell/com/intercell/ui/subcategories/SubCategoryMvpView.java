package intercell.com.intercell.ui.subcategories;

import java.util.List;

import intercell.com.intercell.datasource.model.SubCategories;
import intercell.com.intercell.datasource.model.SubCategoryResponse;
import intercell.com.intercell.ui.base.MvpView;

/**
 * Created by Dell on 1/24/2018.
 */

public interface SubCategoryMvpView extends MvpView {

    void bindSubCategoryList(SubCategoryResponse subCategoriesList);
    void openMentorList();
}
