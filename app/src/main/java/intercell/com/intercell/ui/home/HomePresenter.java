package intercell.com.intercell.ui.home;

import javax.inject.Inject;

import intercell.com.intercell.ui.base.BasePresenter;

/**
 * Created by Dell on 1/19/2018.
 */

public class HomePresenter<V extends HomeMvpView> extends BasePresenter<V>
        implements HomeMvpPresenter<V> {

    @Inject
    HomePresenter(){

    }
}
