package intercell.com.intercell.ui.subcategories;

import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/24/2018.
 */

public interface SubCategoryMvpPresenter<V extends  SubCategoryMvpView> extends MvpPresenter<V> {


    void onGetSubCategory(String id);
}
