package intercell.com.intercell.ui.selecttime;

import java.util.List;

import intercell.com.intercell.datasource.model.SelectTimeResponse;
import intercell.com.intercell.ui.base.MvpView;

/**
 * Created by Dell on 1/31/2018.
 */

public interface SelectTimeMvpView extends MvpView {

    void openOrderDeatil();
    void bindTimeList( SelectTimeResponse selectTimeResponse);

}
