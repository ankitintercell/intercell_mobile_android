package intercell.com.intercell.ui.selecttime;

import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/31/2018.
 */

public interface SelectTimeMvpPresenter<V extends SelectTimeMvpView> extends MvpPresenter<V> {

    void getTimeList(String conusellorId, String startDate, String endDate, String token);

}
