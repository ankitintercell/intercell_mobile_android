package intercell.com.intercell.ui.mentorlist;

/**
 * Created by Dell on 1/25/2018.
 */


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import intercell.com.intercell.datasource.model.BaseResponse;
import intercell.com.intercell.datasource.model.MentorListResponse;
import intercell.com.intercell.datasource.model.SubCategories;
import intercell.com.intercell.datasource.model.SubCategoryResponse;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.ui.base.BasePresenter;
import intercell.com.intercell.utils.AppConstants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MentorListPresenter<V extends MentorListMvpView> extends BasePresenter<V> implements MentorListMvpPresenter<V> {


    @Inject
    ApiInterface apiInterface;

    @Inject
    MentorListPresenter() {

    }

    @Override
    public void onGetMentorList(String selectCategoryId, List<SubCategories> subCategoriesList, String token) {
        getMvpView().showLoading();
        try {
            Map<String, String> params = new HashMap<String, String>();
            params.put("serviceId", selectCategoryId);
            params.put("isGenric", "false");
            params.put("token", token);
            params.put("os", AppConstants.osType);
            params.put("device_id", getMvpView().getDeviceId());


            List<Map<String, String>> selectedList = new ArrayList<Map<String, String>>();
            for (int i = 0; i < subCategoriesList.size(); i++) {
                Map<String, String> subcategoryJson = new HashMap<String, String>();
                SubCategories subCategories = subCategoriesList.get(i);
                subcategoryJson.put("id", subCategories.getId());
                subcategoryJson.put("itemName", subCategories.getItemName());
                selectedList.add(subcategoryJson);

            }

            params.put("subServices", String.valueOf(selectedList));


            apiInterface.getMentorList(params).enqueue(new Callback<BaseResponse<MentorListResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<MentorListResponse>> call, Response<BaseResponse<MentorListResponse>> response) {
                    getMvpView().hideLoading();
                    if (response.body().getStatus() == 0) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        getMvpView().onError(response.body().getMessage());
                        List<MentorListResponse> mentorListResponse = (List<MentorListResponse>) response.body().getResponse();
                        getMvpView().bindMentorList(mentorListResponse);


                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<MentorListResponse>> call, Throwable t) {
                    getMvpView().hideLoading();
                }
            });


        } catch (Exception e) {
            getMvpView().hideLoading();
            e.printStackTrace();
        }

    }
}
