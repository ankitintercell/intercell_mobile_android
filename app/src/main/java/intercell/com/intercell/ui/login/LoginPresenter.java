package intercell.com.intercell.ui.login;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.LoginResponse;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.ui.base.BasePresenter;
import intercell.com.intercell.ui.base.MvpView;
import intercell.com.intercell.utils.AppConstants;
import intercell.com.intercell.utils.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 1/4/2018.
 */

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V> {


    @Inject
    ApiInterface apiService;

    @Inject
    public LoginPresenter() {

    }

    @Override
    public void onServerLoginClick(String email, String password) {

        if (email == null || email.isEmpty()) {
            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.empty_email);
            return;
        }
        if (!CommonUtils.isEmailValid(email)) {
            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.invalid_email);
            return;
        }
        if (password == null || password.isEmpty()) {
            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.empty_password);
            return;
        }
        getMvpView().showLoading();
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("email", email);
            paramObject.put("password", password);
            paramObject.put("os", AppConstants.osType);

            paramObject.put("device_id", getMvpView().getDeviceId());


            //Here the json data is add to a hash map with key data
            Map<String, String> params = new HashMap<String, String>();
            params.put("email", email);
            params.put("password", password);
            params.put("os", AppConstants.osType);

            params.put("device_id", getMvpView().getDeviceId());

            String jstr = paramObject.toString();

            apiService.getUser(params).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    getMvpView().hideLoading();
                    getMvpView().hideKeyboard();
                    if (response.body().getStatus() != 1) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        getMvpView().openMainActivity();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    getMvpView().hideLoading();
                }
            });
        } catch (JSONException e) {

        }
    }


}
