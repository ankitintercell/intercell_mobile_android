package intercell.com.intercell.ui.resetPassword;

import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/16/2018.
 */

public interface ResetPasswordMvpPresenter<V extends  ResetPasswordMvpView> extends MvpPresenter<V> {

    void  onConfirmClick(String userId,String password, String insertId);

}
