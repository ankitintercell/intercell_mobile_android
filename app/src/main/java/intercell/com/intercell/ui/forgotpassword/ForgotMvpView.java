package intercell.com.intercell.ui.forgotpassword;

import intercell.com.intercell.ui.base.MvpView;

/**
 * Created by Dell on 1/10/2018.
 */

public interface ForgotMvpView extends MvpView {

    void  openOTPActivity(String userId,String insertId, String mobile,String token);

}
