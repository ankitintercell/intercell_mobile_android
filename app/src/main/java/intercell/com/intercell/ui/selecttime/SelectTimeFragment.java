package intercell.com.intercell.ui.selecttime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.MentorListResponse;
import intercell.com.intercell.datasource.model.SelectTimeResponse;
import intercell.com.intercell.datasource.model.SubCategories;
import intercell.com.intercell.datasource.model.Time;
import intercell.com.intercell.di.component.ActivityComponent;
import intercell.com.intercell.ui.base.BaseFragment;
import intercell.com.intercell.ui.confirm_order.ConfirmOrderFragment;
import intercell.com.intercell.utils.AppConstants;

/**
 * Created by Dell on 1/31/2018.
 */

public class SelectTimeFragment extends BaseFragment implements SelectTimeMvpView {


    public static final String TAG = "SelectTime";
    protected View mView;
    TextView txt_name, txt_experience, txt_field, txt_brand;
    Button btn_proceed;
    ImageView img_mentor;
    MentorListResponse mentorListResponse;
    SelectTimeAdapter selectTimeAdapter;
    List<Time> selectTimeResponses = new ArrayList<>();
    String startDate = "", endDate = "";
    RecyclerView lst_time;


    @Inject
    SelectTimePresenter<SelectTimeMvpView> selectTimeMvpViewSelectTimePresenter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mentorListResponse = (MentorListResponse) getArguments().getSerializable("selectedMentor");
        startDate=getArguments().getString("date");
        View view = inflater.inflate(R.layout.select_time, container, false);
        this.mView = view;

        ActivityComponent activityComponent = getActivityComponent();
        if (activityComponent != null) {
            try {


                setUp(mView);
                activityComponent.inject(this);
                selectTimeMvpViewSelectTimePresenter.onAttach(this);
                selectTimeMvpViewSelectTimePresenter.getTimeList(mentorListResponse.getId(), startDate, startDate, AppConstants.Token);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return mView;
    }

    @Override
    public String getDeviceId() {
        return null;
    }

    @Override
    protected void setUp(View view) {
        img_mentor = (ImageView) view.findViewById(R.id.img_mentor);
        txt_name = (TextView) view.findViewById(R.id.txt_name);
        txt_experience = (TextView) view.findViewById(R.id.txt_experience);
        txt_field = (TextView) view.findViewById(R.id.txt_field);
        txt_brand = (TextView) view.findViewById(R.id.txt_brand);
        lst_time = (RecyclerView) view.findViewById(R.id.lst_time);
        btn_proceed=(Button) view.findViewById(R.id.btn_proceed);
        txt_name.setText(mentorListResponse.getFirstName() + " " + mentorListResponse.getLastName());
        txt_experience.setText(mentorListResponse.getTotalexperience());
        txt_brand.setText(mentorListResponse.getBrandWorkedFor());
        txt_field.setText(mentorListResponse.getCategory());

        Picasso.with(getBaseActivity()).load(AppConstants.ImageBaseUrl + mentorListResponse.getDpImage())
                .into(img_mentor);

        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getBaseActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ConfirmOrderFragment fragment = new ConfirmOrderFragment();
                Bundle bun = new Bundle();
                bun.putSerializable("selectedMentor", (Serializable) mentorListResponse);

                fragment.setArguments(bun);
                fragmentTransaction.add(R.id.containerView, fragment, SelectTimeFragment.TAG);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


    }

    @Override
    public void openOrderDeatil() {

    }

    @Override
    public void bindTimeList(SelectTimeResponse selectTimeResponseList) {
        for (int i = 0; i < selectTimeResponseList.getTime().size(); i++) {
            Time time = selectTimeResponseList.getTime().get(i);
            selectTimeResponses.add(time);

        }

        selectTimeAdapter = new SelectTimeAdapter(getBaseActivity(), selectTimeResponses);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getBaseActivity(), 3);
        lst_time.setLayoutManager(mLayoutManager);
        lst_time.setItemAnimator(new DefaultItemAnimator());
        lst_time.setAdapter(selectTimeAdapter);
    }
}
