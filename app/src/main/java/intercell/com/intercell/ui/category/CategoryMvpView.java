package intercell.com.intercell.ui.category;


import java.util.List;

import intercell.com.intercell.datasource.model.CategoryResponse;
import intercell.com.intercell.ui.base.MvpView;

/**
 * Created by Dell on 1/23/2018.
 */

public interface CategoryMvpView extends MvpView {

    void openSubCategory(String id);
    void bindCategoryList(List<CategoryResponse> categoryResponseList);
}
