package intercell.com.intercell.ui.forgotpassword;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.BaseResponse;
import intercell.com.intercell.datasource.model.ForgotResponse;
import intercell.com.intercell.datasource.model.LoginResponse;
import intercell.com.intercell.datasource.model.SignupResponse;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.ui.base.BasePresenter;
import intercell.com.intercell.utils.AppConstants;
import intercell.com.intercell.utils.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 1/10/2018.
 */

public class ForgotPresenter<V extends ForgotMvpView> extends BasePresenter<V> implements ForgotMvpPresenter<V> {

    @Inject
    ApiInterface apiInterface;

    @Inject
    public  ForgotPresenter(){

    }

    @Override
    public void onServerApiCall(String email) {

        if (email == null || email.isEmpty()) {
            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.empty_email);
            return;
        }
        if (!CommonUtils.isEmailValid(email)) {
            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.invalid_email);
            return;
        }
        getMvpView().showLoading();
        try {



            //Here the json data is add to a hash map with key data
            Map<String,String> params = new HashMap<String, String>();
            params.put("email", email);

            params.put("os", AppConstants.osType);

            params.put("device_id", getMvpView().getDeviceId());



            apiInterface.getFogotassword(params).enqueue(new Callback<BaseResponse<ForgotResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<ForgotResponse>> call, Response<BaseResponse<ForgotResponse>> response) {
                    getMvpView().hideLoading();
                    getMvpView().hideKeyboard();
                    if (response.body().getStatus() != 1) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        ForgotResponse forgotResponse = (ForgotResponse) response.body().getResponse().get(0);
                        getMvpView().openOTPActivity(forgotResponse.getUserId(),forgotResponse.getInsertId().toString(),
                                forgotResponse.getMobile(),forgotResponse.getToken());
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<ForgotResponse>>call, Throwable t) {
                    getMvpView().hideLoading();

                }
            });
        } catch (Exception e) {

        }

    }
}
