package intercell.com.intercell.ui.tc;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import intercell.com.intercell.datasource.model.BaseResponse;
import intercell.com.intercell.datasource.model.TcResponse;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 1/12/2018.
 */

public class TcPresenter<V extends TcMvpView> extends BasePresenter<V> implements TcMvpPresenter<V> {
    @Inject
    ApiInterface apiInterface;


    @Inject
    public TcPresenter() {

    }


    @Override
    public void onTcCall() {

        getMvpView().showLoading();
        try {


            //Here the json data is add to a hash map with key data
            Map<String,String> params = new HashMap<String, String>();
            params.put("short_code", "terms");

            apiInterface.getTc(params).enqueue(new Callback<BaseResponse<TcResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<TcResponse>> call, Response<BaseResponse<TcResponse>> response) {
                    getMvpView().hideLoading();

                    if (response.body().getStatus() != 1) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        TcResponse tcResponse = (TcResponse) response.body().getResponse().get(0);


                        getMvpView().showData(tcResponse.getData().getCmsDesc());
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<TcResponse>> call, Throwable t) {

                }
            });
        } catch (Exception e) {

        }

    }
}
