package intercell.com.intercell.ui.main;

import android.os.Bundle;

import android.support.annotation.NonNull;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import intercell.com.intercell.R;
import intercell.com.intercell.ui.base.BaseActivity;
import intercell.com.intercell.ui.custom.EndDrawerToggle;
import intercell.com.intercell.ui.home.HomeFragment;

/**
 * Created by Dell on 1/17/2018.
 */

public class MainActivity extends BaseActivity implements MainMvpView {

    @Inject
    MainPresenter<MainMvpView> mainMvpViewMainPresenter;

    @BindView(R.id.toolbar)
   public  Toolbar mToolbar;

    @BindView(R.id.drawer_view)
    DrawerLayout mDrawer;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    private EndDrawerToggle mDrawerToggle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_main);
        mainMvpViewMainPresenter.onAttach(this);

        ButterKnife.bind(this);

        setUp();


    }


    protected void setUp() {

        setSupportActionBar(mToolbar);
        mDrawerToggle = new EndDrawerToggle(
                this,
                mDrawer,
                mToolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, 0); // this disables the animation
            }

        };
        mDrawer.addDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();




        setupNavMenu();
        //mPresenter.onNavMenuCreated();
        //  setupCardContainerView();
        // mPresenter.onViewInitialized();

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawer.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawer.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawer.openDrawer(Gravity.RIGHT);
                }
            }
        });



    }


    void setupNavMenu() {
//        View headerLayout = mNavigationView.getHeaderView(0);
//        mProfileImageView = (RoundedImageView) headerLayout.findViewById(R.id.iv_profile_pic);
//        mNameTextView = (TextView) headerLayout.findViewById(R.id.tv_name);
//        mEmailTextView = (TextView) headerLayout.findViewById(R.id.tv_email);
        showHomeFragment();

        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        mDrawer.closeDrawer(GravityCompat.END);
                        switch (item.getItemId()) {
                            case R.id.nav_item_home:
                                closeNavigationDrawer();
                                showHomeFragment();
                                return true;
//                            case R.id.nav_item_rate_us:
//                                mPresenter.onDrawerRateUsClick();
//                                return true;
//                            case R.id.nav_item_feed:
//                                mPresenter.onDrawerMyFeedClick();
//                                return true;
//                            case R.id.nav_item_logout:
//                                mPresenter.onDrawerOptionLogoutClick();
//                                return true;
                            default:
                                closeNavigationDrawer();
                                return false;
                        }
                    }
                });
    }

    @Override
    public void closeNavigationDrawer() {
        if (mDrawer != null) {
            mDrawer.closeDrawer(Gravity.END);
        }
    }

    @Override
    public void lockDrawer() {

    }

    @Override
    public void unlockDrawer() {

    }

    @Override
    public void showHomeFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .disallowAddToBackStack()
//                .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                .add(R.id.containerView, HomeFragment.newInstance(), HomeFragment.TAG)
                .commit();
    }

    @Override
    public void onBackPressed() {

        if (mDrawer.isDrawerOpen(GravityCompat.END)) {
            mDrawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }

    }
}
