package intercell.com.intercell.ui.selecttime;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.Time;

/**
 * Created by Dell on 1/31/2018.
 */

public class SelectTimeAdapter extends RecyclerView.Adapter<SelectTimeAdapter.MyViewHolder> {

    private List<Time> mTimeList;
    private int lastCheckedPosition = -1;
    Activity activity;

    public SelectTimeAdapter(Activity activity, List<Time> TimeList) {
        mTimeList = TimeList;
        this.activity=activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_list_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Time Time = mTimeList.get(position);

        if(Time.getStatus().equals("Not Available")){
            holder.ll_view.setBackgroundResource(R.color.gray);
            holder.ll_view.setClickable(false);
        }else{
            holder.ll_view.setBackgroundResource(R.color.white);

            holder.ll_view.setClickable(true);
        }
        if(lastCheckedPosition==position && Time.getStatus().equals("Available")){
            holder.ll_view.setBackgroundResource(R.color.blue_theme_color);

        }else if(Time.getStatus().equals("Available")){
            holder.ll_view.setBackgroundResource(R.color.white);

        }

        holder.txt_time.setText(Time.getStart()+"-"+Time.getEnd());

        holder.ll_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastCheckedPosition = position;
                notifyItemRangeChanged(0, mTimeList.size());
            }
        });

    }


    public Time getSelectedItem(){
        Time model = mTimeList.get(lastCheckedPosition);
        return model;
    }
    public int selectedPosition(){
        return lastCheckedPosition;
    }

    @Override
    public int getItemCount() {
        return mTimeList == null ? 0 : mTimeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private View view;
        private LinearLayout ll_view;
        private TextView txt_time;

        private MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ll_view = (LinearLayout) itemView.findViewById(R.id.ll_view);
            txt_time = (TextView) itemView.findViewById(R.id.txt_time);

        }
    }
}

