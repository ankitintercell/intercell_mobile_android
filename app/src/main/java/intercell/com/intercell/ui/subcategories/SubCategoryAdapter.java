package intercell.com.intercell.ui.subcategories;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.SubCategories;

/**
 * Created by Dell on 1/24/2018.
 */

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder> {

    private List<SubCategories> mSubCategoriesList;

    public SubCategoryAdapter(List<SubCategories> SubCategoriesList) {
        mSubCategoriesList = SubCategoriesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_category_list_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final SubCategories SubCategories = mSubCategoriesList.get(position);
        holder.txt_subcategor.setText(SubCategories.getItemName());
        holder.chk_sub_category.setChecked(SubCategories.isSelected() ? true : false);


        holder.chk_sub_category.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SubCategories.setSelected(!SubCategories.isSelected());
                holder.chk_sub_category.setChecked(SubCategories.isSelected() ? true : false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSubCategoriesList == null ? 0 : mSubCategoriesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private View view;
        private CheckBox chk_sub_category;
        private TextView txt_subcategor;

        private MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            chk_sub_category=(CheckBox) itemView.findViewById(R.id.chk_sub_category);
            txt_subcategor = (TextView) itemView.findViewById(R.id.txt_subcategor);
        }
    }
}
