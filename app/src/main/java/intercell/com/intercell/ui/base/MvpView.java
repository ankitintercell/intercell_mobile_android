package intercell.com.intercell.ui.base;

import android.support.annotation.StringRes;

/**
 * Created by Dell on 12/14/2017.
 */

public interface MvpView {

    void showLoading();

    void hideLoading();

    void openActivityOnTokenExpire();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean isNetworkConnected();

    void hideKeyboard();

    String getDeviceId();

}
