package intercell.com.intercell.ui.subcategories;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import intercell.com.intercell.datasource.model.BaseResponse;
import intercell.com.intercell.datasource.model.SubCategories;
import intercell.com.intercell.datasource.model.SubCategoryResponse;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 1/24/2018.
 */

public class SubCategoryPresenter<V extends SubCategoryMvpView> extends BasePresenter<V>
        implements SubCategoryMvpPresenter<V> {

    @Inject
    ApiInterface apiInterface;


    @Inject
    SubCategoryPresenter() {

    }


    @Override
    public void onGetSubCategory(String id) {
        getMvpView().showLoading();

        try {


            apiInterface.getSubCategoryList(id).enqueue(new Callback<BaseResponse<SubCategoryResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<SubCategoryResponse>> call, Response<BaseResponse<SubCategoryResponse>> response) {
                    getMvpView().hideLoading();
                    if (response.body().getStatus() == 0) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        getMvpView().onError(response.body().getMessage());
                        SubCategoryResponse subCategoryResponse = (SubCategoryResponse) response.body().getResponse().get(0);

                        getMvpView().bindSubCategoryList(subCategoryResponse);

                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<SubCategoryResponse>> call, Throwable t) {
                    getMvpView().hideLoading();
                }
            });


        } catch (Exception e) {
            getMvpView().hideLoading();
            e.printStackTrace();
        }
    }
}
