package intercell.com.intercell.ui.mentorlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.MentorListResponse;
import intercell.com.intercell.datasource.model.SubCategories;
import intercell.com.intercell.di.component.ActivityComponent;
import intercell.com.intercell.ui.base.BaseFragment;

/**
 * Created by Dell on 1/25/2018.
 */

public class MentorListFragment extends BaseFragment implements MentorListMvpView {
    protected View mView;
    @Inject
    MentorListPresenter<MentorListMvpView> mentorListMvpViewMentorListPresenter;

    public static final String TAG = "MentorList";
    MentorListAdapter mentorListAdapter;
    RecyclerView lst_mentor;
    TextView txt_field_name;

    List<SubCategories> subCategoriesList = new ArrayList<>();
    String selectCategoryId = "";

    List<MentorListResponse> mentorListResponseArrayList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        subCategoriesList = (List<SubCategories>) getArguments().getSerializable("selectedIdList");
        selectCategoryId = getArguments().getString("selectCategoryId");
        View view = inflater.inflate(R.layout.mentor_list, container, false);
        this.mView = view;
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            try {
                txt_field_name = (TextView) mView.findViewById(R.id.txt_field_name);
                lst_mentor = (RecyclerView) mView.findViewById(R.id.lst_mentor);
                component.inject(this);
                mentorListMvpViewMentorListPresenter.onAttach(this);
                mentorListMvpViewMentorListPresenter.onGetMentorList(selectCategoryId, subCategoriesList, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Ijc3In0.0wd6KX_CEQ00phI-vhH58-kTmfEmI39DX7rVH4oKdC4");


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return view;
    }


    @Override
    public String getDeviceId() {
        return null;
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void openMentorDetail() {

    }

    @Override
    public void openSelectDate() {

    }

    @Override
    public void bindMentorList(List<MentorListResponse> mentorListResponseList) {

        for (int i = 0; i < mentorListResponseList.size(); i++) {
            MentorListResponse mentorListResponse = mentorListResponseList.get(i);
            txt_field_name.setText(mentorListResponse.getCategory_name());
            mentorListResponseArrayList.add(mentorListResponse);

        }

        mentorListAdapter = new MentorListAdapter(mentorListResponseArrayList,getBaseActivity());

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getBaseActivity(), 1);
        lst_mentor.setLayoutManager(mLayoutManager);
        lst_mentor.setItemAnimator(new DefaultItemAnimator());
        lst_mentor.setAdapter(mentorListAdapter);

    }


}
