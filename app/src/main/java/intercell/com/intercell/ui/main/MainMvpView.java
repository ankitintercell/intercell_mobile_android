package intercell.com.intercell.ui.main;

import intercell.com.intercell.ui.base.MvpView;

/**
 * Created by Dell on 1/17/2018.
 */

public interface MainMvpView extends MvpView {

    void closeNavigationDrawer();

    void lockDrawer();

    void unlockDrawer();

    void showHomeFragment();
}
