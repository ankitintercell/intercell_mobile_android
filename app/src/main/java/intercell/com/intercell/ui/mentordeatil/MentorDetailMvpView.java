package intercell.com.intercell.ui.mentordeatil;

import intercell.com.intercell.datasource.model.MentorDetailResponse;
import intercell.com.intercell.ui.base.MvpView;

/**
 * Created by Dell on 1/30/2018.
 */

public interface MentorDetailMvpView extends MvpView {

    void bindMentorDeatil(MentorDetailResponse mentorDetailResponse);


}
