package intercell.com.intercell.ui.confirm_order;

import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/31/2018.
 */

public interface ConfirmOrderMvpPresenter<V extends ConfirmOrderMvpView> extends MvpPresenter<V> {

}
