package intercell.com.intercell.ui.base;

/**
 * Created by Dell on 12/14/2017.
 */

public interface MvpPresenter<V extends MvpView> {
    void onAttach(V mvpView);

    void onDetach();

//    void handleApiError(ANError error);

    void setUserAsLoggedOut();
}
