package intercell.com.intercell.ui.otp;

import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/15/2018.
 */

public interface OtpMvpPresenter<V extends  OtpMvpView> extends MvpPresenter<V> {

    void onServerOtpClick(String phone,String token);

    void onconfirmOtpClick(String userId,String key);

}
