package intercell.com.intercell.ui.resetPassword;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.BaseResponse;
import intercell.com.intercell.datasource.model.SignupResponse;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.ui.base.BasePresenter;
import intercell.com.intercell.utils.AppConstants;
import intercell.com.intercell.utils.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 1/16/2018.
 */

public class ResetPasswordPresenter<V extends ResetPasswordMvpView> extends BasePresenter<V> implements ResetPasswordMvpPresenter<V> {


    @Inject
    ApiInterface apiInterface;
   @Inject
   public  ResetPasswordPresenter(){

   }

    @Override
    public void onConfirmClick(String userId, String password, String insertId) {

        if (password == null || password.isEmpty()) {
            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.empty_password);
            return;
        }
        if (!CommonUtils.isValidPassword(password)) {
            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.invalid_password);
            return;
        }

        getMvpView().showLoading();

        try {


            //Here the json data is add to a hash map with key data
            Map<String, String> params = new HashMap<String, String>();
            params.put("userId", userId);
            params.put("password", password);
            params.put("insertId", insertId);

            params.put("os", AppConstants.osType);
            params.put("device_id", getMvpView().getDeviceId());

            apiInterface.setNewPassword(params).enqueue(new Callback<BaseResponse<SignupResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<SignupResponse>> call, Response<BaseResponse<SignupResponse>> response) {
                    getMvpView().hideLoading();

                    if (response.body().getStatus() == 0) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        SignupResponse signupResponse = (SignupResponse) response.body().getResponse().get(0);


                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<SignupResponse>> call, Throwable t) {
                    getMvpView().hideLoading();

                }
            });
        } catch (Exception e) {

        }
    }
}
