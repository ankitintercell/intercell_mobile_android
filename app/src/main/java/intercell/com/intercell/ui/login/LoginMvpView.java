package intercell.com.intercell.ui.login;

import intercell.com.intercell.ui.base.MvpView;

/**
 * Created by Dell on 1/3/2018.
 */

public interface LoginMvpView extends MvpView{
    void openMainActivity();

}
