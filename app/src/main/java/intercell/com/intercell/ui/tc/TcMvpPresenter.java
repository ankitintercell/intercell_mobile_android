package intercell.com.intercell.ui.tc;

import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/12/2018.
 */

public interface TcMvpPresenter<V extends TcMvpView> extends MvpPresenter<V> {

    void onTcCall();

}
