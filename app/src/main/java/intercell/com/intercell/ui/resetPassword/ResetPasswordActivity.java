package intercell.com.intercell.ui.resetPassword;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import intercell.com.intercell.R;
import intercell.com.intercell.ui.base.BaseActivity;
import intercell.com.intercell.ui.login.LoginActivity;
import intercell.com.intercell.ui.main.MainActivity;

/**
 * Created by Dell on 1/16/2018.
 */

public class ResetPasswordActivity extends BaseActivity implements ResetPasswordMvpView {


    @Inject
    ResetPasswordPresenter<ResetPasswordMvpView> resetPasswordMvpViewResetPasswordPresenter;

    @BindView(R.id.et_old_password)
    EditText et_old_password;

    @BindView(R.id.et_new_password)
    EditText et_new_password;

    @BindView(R.id.et_confirm_password)
    EditText et_confirm_password;


    @BindView(R.id.btn_confirm)
    Button btn_confirm;

    String userId = "", insertId = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.reset_password);
        resetPasswordMvpViewResetPasswordPresenter.onAttach(this);
        ButterKnife.bind(this);
        userId = getIntent().getStringExtra("userId");
        insertId = getIntent().getStringExtra("insertId");


    }


    @OnClick(R.id.btn_confirm)
    public void onConfirmClick(View v) {
        resetPasswordMvpViewResetPasswordPresenter.onConfirmClick(userId, et_new_password.getText().toString(), insertId);
    }

    @Override
    public void openMainAcitvity() {
        Intent in = new Intent(ResetPasswordActivity.this, MainActivity.class);
        startActivity(in);
        finish();

    }
}
