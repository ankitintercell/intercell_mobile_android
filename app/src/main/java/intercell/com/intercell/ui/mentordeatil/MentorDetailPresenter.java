package intercell.com.intercell.ui.mentordeatil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import intercell.com.intercell.datasource.model.BaseResponse;
import intercell.com.intercell.datasource.model.MentorDetailResponse;
import intercell.com.intercell.datasource.model.MentorListResponse;
import intercell.com.intercell.datasource.model.SubCategories;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.ui.base.BasePresenter;
import intercell.com.intercell.utils.AppConstants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 1/30/2018.
 */

public class MentorDetailPresenter<V extends MentorDetailMvpView> extends BasePresenter<V> implements MentorDetailMvpPresenter<V> {


    @Inject
    ApiInterface apiInterface;

    @Inject
    MentorDetailPresenter() {

    }

    @Override
    public void getMentorDeatil(String userId, String token) {

        getMvpView().showLoading();
        try {


            apiInterface.getMentorDeatil(userId + "/" + token).enqueue(new Callback<BaseResponse<MentorDetailResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<MentorDetailResponse>> call, Response<BaseResponse<MentorDetailResponse>> response) {
                    getMvpView().hideLoading();
                    if (response.body().getStatus() == 0) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        getMvpView().onError(response.body().getMessage());
                        MentorDetailResponse mentorDetailResponse = (MentorDetailResponse) response.body().getResponse().get(0);

                        getMvpView().bindMentorDeatil(mentorDetailResponse);


                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<MentorDetailResponse>> call, Throwable t) {
                    getMvpView().hideLoading();
                }
            });


        } catch (Exception e) {
            getMvpView().hideLoading();
            e.printStackTrace();
        }


    }
}
