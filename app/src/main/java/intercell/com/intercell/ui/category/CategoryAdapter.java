package intercell.com.intercell.ui.category;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.CategoryResponse;

/**
 * Created by Dell on 1/23/2018.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {


    private List<CategoryResponse> categoryList;

    public CategoryAdapter(List<CategoryResponse> categoryList) {
        this.categoryList = categoryList;
    }

    @Override
    public CategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoryAdapter.MyViewHolder holder, int position) {
        CategoryResponse categoryResponse = categoryList.get(position);
        if (categoryResponse.getName().equals("Sales"))
            holder.img_category.setBackgroundResource(R.drawable.cat_sales);
        if (categoryResponse.getName().equals("Marketing"))
            holder.img_category.setBackgroundResource(R.drawable.cat_marketing);
        if (categoryResponse.getName().equals("Entrepreneurship"))
            holder.img_category.setBackgroundResource(R.drawable.cat_enterpreurship);

        if (categoryResponse.getName().contains("Human"))
            holder.img_category.setBackgroundResource(R.drawable.cat_human_resoruce);
        holder.txt_category_name.setText(categoryResponse.getName());


    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_category_name;
        public ImageView img_category;

        public MyViewHolder(View view) {
            super(view);
            txt_category_name = (TextView) view.findViewById(R.id.txt_category_name);
            img_category = (ImageView) view.findViewById(R.id.img_category);

        }
    }
}
