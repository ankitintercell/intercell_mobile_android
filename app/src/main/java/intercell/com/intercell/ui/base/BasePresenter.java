package intercell.com.intercell.ui.base;

import javax.inject.Inject;

/**
 * Created by Dell on 12/20/2017.
 */

public class BasePresenter<V extends  MvpView> implements MvpPresenter<V> {


    private static final String TAG = "BasePresenter";

    private V mMvpView;

    @Inject
    public BasePresenter(){

    }

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mMvpView = null;
    }


    public V getMvpView() {
        return mMvpView;
    }


    public boolean isViewAttached() {
        return mMvpView != null;
    }


    @Override
    public void setUserAsLoggedOut() {

    }
}
