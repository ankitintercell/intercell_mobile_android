package intercell.com.intercell.ui.category;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import intercell.com.intercell.datasource.model.BaseResponse;
import intercell.com.intercell.datasource.model.CategoryResponse;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 1/23/2018.
 */

public class CategoryPresenter<V extends CategoryMvpView> extends BasePresenter<V>
        implements CategoryMvpPresenter<V> {

    @Inject
    ApiInterface apiInterface;

    List<CategoryResponse> categoryResponseList = new ArrayList<>();


    @Inject
    CategoryPresenter() {

    }

    @Override
    public void onserverclick() {

        getMvpView().showLoading();

        try {


            apiInterface.getCategoryList().enqueue(new Callback<BaseResponse<CategoryResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<CategoryResponse>> call, Response<BaseResponse<CategoryResponse>> response) {
                    getMvpView().hideLoading();
                    if (response.body().getStatus() == 0) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        getMvpView().onError(response.body().getMessage());
                        for (int i = 0; i < response.body().getResponse().size(); i++) {
                            CategoryResponse categoryResponse = (CategoryResponse) response.body().getResponse().get(i);
                            categoryResponseList.add(categoryResponse);
                        }
                                               getMvpView().bindCategoryList(categoryResponseList);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<CategoryResponse>> call, Throwable t) {
                    getMvpView().hideLoading();
                }
            });


        } catch (Exception e) {

        }

    }
}
