package intercell.com.intercell.ui.mentordeatil;

import intercell.com.intercell.ui.base.BasePresenter;
import intercell.com.intercell.ui.base.MvpPresenter;
import intercell.com.intercell.ui.mentorlist.MentorListMvpView;

/**
 * Created by Dell on 1/30/2018.
 */

public interface MentorDetailMvpPresenter<V extends MentorDetailMvpView> extends MvpPresenter<V> {

    void getMentorDeatil(String userId, String token);

}
