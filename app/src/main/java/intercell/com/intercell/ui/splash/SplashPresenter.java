package intercell.com.intercell.ui.splash;

import javax.inject.Inject;

import intercell.com.intercell.ui.base.BasePresenter;

/**
 * Created by Dell on 12/21/2017.
 */

public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V> implements SplashMvpPresenter<V>  {

    @Inject
    public SplashPresenter() {

    }

    @Override
    public void swithScreen() {
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               getMvpView().openLoginActivity();
            }
        }, 3000); // delay 3 secondsgetMvpView().openMainActivity();
    }
}
