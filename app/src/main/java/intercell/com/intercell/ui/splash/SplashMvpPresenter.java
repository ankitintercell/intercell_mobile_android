package intercell.com.intercell.ui.splash;

import intercell.com.intercell.di.annotation.PerActivity;
import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 12/20/2017.
 */
@PerActivity
public interface SplashMvpPresenter<V extends  SplashMvpView> extends MvpPresenter<V> {
    void swithScreen();
}
