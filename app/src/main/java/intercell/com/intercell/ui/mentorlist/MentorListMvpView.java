package intercell.com.intercell.ui.mentorlist;

import org.json.JSONArray;

import java.util.List;

import intercell.com.intercell.datasource.model.MentorListResponse;
import intercell.com.intercell.ui.base.MvpView;

/**
 * Created by Dell on 1/25/2018.
 */

public interface MentorListMvpView extends MvpView {

    void openMentorDetail();

    void openSelectDate();

    void bindMentorList(List<MentorListResponse> mentorListResponse);
}
