package intercell.com.intercell.ui.mentordeatil;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;

import javax.inject.Inject;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.MentorDetailResponse;
import intercell.com.intercell.datasource.model.MentorListResponse;
import intercell.com.intercell.di.component.ActivityComponent;
import intercell.com.intercell.ui.base.BaseFragment;
import intercell.com.intercell.ui.selectdate.SelectDateFragment;
import intercell.com.intercell.utils.AppConstants;

/**
 * Created by Dell on 1/30/2018.
 */

public class MentorDetailFragment extends BaseFragment implements MentorDetailMvpView {


    protected View mView;
    public static final String TAG = "MentorDetail";
    MentorListResponse mentorListResponse;

    @Inject
    MentorDetailPresenter<MentorDetailMvpView> mentorDetailMvpViewMentorDetailPresenter;

    ImageView img_mentor;
    TextView txt_name, txt_experience, txt_field, txt_brand, txt_description;
    Button btn_book_session;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mentorListResponse = (MentorListResponse) getArguments().getSerializable("selectedMentor");
        try {
            View view = inflater.inflate(R.layout.mentor_detail, container, false);
            this.mView = view;
        } catch (Exception e) {
            e.printStackTrace();
        }
        ActivityComponent component = getActivityComponent();
        if (component != null) {


            setUp(mView);

            component.inject(this);
            getBaseActivity().setTitle("Select a Stream");
            mentorDetailMvpViewMentorDetailPresenter.onAttach(this);
            mentorDetailMvpViewMentorDetailPresenter.getMentorDeatil(mentorListResponse.getId(), AppConstants.Token);


        }
        return mView;
    }

    @Override
    public String getDeviceId() {
        return null;
    }

    @Override
    protected void setUp(View view) {
        txt_name = (TextView) view.findViewById(R.id.txt_name);
        img_mentor = (ImageView) view.findViewById(R.id.img_mentor);
        txt_experience = (TextView) view.findViewById(R.id.txt_experience);
        txt_field = (TextView) view.findViewById(R.id.txt_field);
        txt_brand = (TextView) view.findViewById(R.id.txt_brand);
        txt_description = (TextView) view.findViewById(R.id.txt_description);
        btn_book_session = (Button) view.findViewById(R.id.btn_book_session);


        btn_book_session.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getBaseActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SelectDateFragment fragment = new SelectDateFragment();
                Bundle bun = new Bundle();
                bun.putSerializable("selectedMentor", (Serializable) mentorListResponse);

                fragment.setArguments(bun);
                fragmentTransaction.add(R.id.containerView, fragment, TAG);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public void bindMentorDeatil(MentorDetailResponse mentorDetailResponse) {

        txt_name.setText(mentorDetailResponse.getFirstName() + " " + mentorDetailResponse.getLastName());
        txt_experience.setText(mentorDetailResponse.getTotalexperience());
        txt_brand.setText(mentorDetailResponse.getBrandWorkedFor());
        txt_field.setText(mentorDetailResponse.getCategory());
        txt_description.setText(mentorDetailResponse.getSummary());

    }
}
