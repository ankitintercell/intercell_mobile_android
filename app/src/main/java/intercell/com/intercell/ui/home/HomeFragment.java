package intercell.com.intercell.ui.home;


import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import intercell.com.intercell.R;
import intercell.com.intercell.di.component.ActivityComponent;
import intercell.com.intercell.ui.base.BaseFragment;
import intercell.com.intercell.ui.category.CategoryFragment;


/**
 * Created by Dell on 1/19/2018.
 */

public class HomeFragment extends BaseFragment implements HomeMvpView {


    @Inject
    HomePresenter<HomeMvpView> homeMvpViewHomePresenter;

    String selectedid="";


    @BindView(R.id.lay_notifications)
    LinearLayout lay_notifications;

    @BindView(R.id.lay_sessions)
    LinearLayout lay_sessions;

    @BindView(R.id.lay_my_profile)
    LinearLayout lay_my_profile;

    @BindView(R.id.lay_book_session)
    LinearLayout lay_book_session;


    public static final String TAG = "Home";


    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            ButterKnife.bind(this, view);
            homeMvpViewHomePresenter.onAttach(this);
        }

        return view;
    }



    @OnClick(R.id.lay_book_session)
    public void onBookSessionClick(){

        FragmentManager fragmentManager = getBaseActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

//        fragmentManager
//                .beginTransaction().disallowAddToBackStack().add(R.id.containerView, CategoryFragment.newInstance(), CategoryFragment.TAG)
//                .commit();

        CategoryFragment fragment3 = new CategoryFragment();
        Bundle bun= new Bundle();
        bun.putString("selectedid",selectedid);
        fragment3.setArguments(bun);
        fragmentTransaction.add(R.id.containerView, fragment3,CategoryFragment.TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();


    }


    @Override
    protected void setUp(View view) {

    }

    @Override
    public String getDeviceId() {
        return null;
    }


    @Override
    public void onDestroyView() {
        homeMvpViewHomePresenter.onDetach();
        super.onDestroyView();
    }
}
