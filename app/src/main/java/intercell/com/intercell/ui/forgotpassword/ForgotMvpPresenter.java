package intercell.com.intercell.ui.forgotpassword;

import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/10/2018.
 */

public interface ForgotMvpPresenter<V extends ForgotMvpView> extends MvpPresenter<V> {


    void onServerApiCall(String email);
}
