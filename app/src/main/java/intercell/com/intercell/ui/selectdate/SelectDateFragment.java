package intercell.com.intercell.ui.selectdate;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.MentorListResponse;
import intercell.com.intercell.datasource.model.SubCategories;
import intercell.com.intercell.di.component.ActivityComponent;
import intercell.com.intercell.ui.base.BaseFragment;
import intercell.com.intercell.ui.selecttime.SelectTimeFragment;
import intercell.com.intercell.utils.AppConstants;

/**
 * Created by Dell on 1/29/2018.
 */

public class SelectDateFragment extends BaseFragment implements SelectDateMvpView {

    public static final String TAG = "SelectDate";
    protected View mView;
    CalendarView calender;
    ImageView img_mentor;
    MentorListResponse mentorListResponse;
    TextView txt_name,txt_experience,txt_field,txt_brand;

    @Inject
    SelectDatePresenter<SelectDateMvpView> selectDateMvpViewSelectDatePresenter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mentorListResponse = (MentorListResponse) getArguments().getSerializable("selectedMentor");
        View view = inflater.inflate(R.layout.select_date, container, false);
        this.mView = view;
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            try {


                setUp(mView);
                component.inject(this);

                selectDateMvpViewSelectDatePresenter.onAttach(this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return mView;
    }

    @Override
    public String getDeviceId() {
        return null;
    }

    @Override
    protected void setUp(View view) {
        calender = (CalendarView) view.findViewById(R.id.calender);
        img_mentor=(ImageView) view.findViewById(R.id.img_mentor);
        txt_name=(TextView) view.findViewById(R.id.txt_name);
        txt_experience=(TextView) view.findViewById(R.id.txt_experience);
        txt_field=(TextView) view.findViewById(R.id.txt_field);
        txt_brand=(TextView) view.findViewById(R.id.txt_brand);

        txt_name.setText(mentorListResponse.getFirstName() + " " + mentorListResponse.getLastName());
        txt_experience.setText(mentorListResponse.getTotalexperience());
        txt_brand.setText(mentorListResponse.getBrandWorkedFor());
        txt_field.setText(mentorListResponse.getCategory());
        Picasso.with(getBaseActivity()).load(AppConstants.ImageBaseUrl + mentorListResponse.getDpImage()).into(img_mentor);



        calender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {


                FragmentManager fragmentManager = getBaseActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SelectTimeFragment fragment = new SelectTimeFragment();
                Bundle bun = new Bundle();
                bun.putSerializable("selectedMentor", (Serializable) mentorListResponse);
                bun.putString("date",String.valueOf(year+"-"+((month<10)?("0"+(month+1)):(month+1))+"-"+dayOfMonth));

                fragment.setArguments(bun);
                fragmentTransaction.add(R.id.containerView, fragment, SelectTimeFragment.TAG);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }
}
