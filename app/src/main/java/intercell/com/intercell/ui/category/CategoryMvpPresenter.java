package intercell.com.intercell.ui.category;

import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/23/2018.
 */

public interface CategoryMvpPresenter<V extends CategoryMvpView> extends MvpPresenter<V> {

    void onserverclick();
}
