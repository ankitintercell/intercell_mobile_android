package intercell.com.intercell.ui.category;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.CategoryResponse;
import intercell.com.intercell.di.component.ActivityComponent;
import intercell.com.intercell.ui.base.BaseFragment;
import intercell.com.intercell.ui.custom.RecyclerItemClickSupport;
import intercell.com.intercell.ui.subcategories.SubCategoryFragment;

/**
 * Created by Dell on 1/23/2018.
 */

public class CategoryFragment extends BaseFragment implements CategoryMvpView {

    protected View mView;


    @Inject
    CategoryPresenter<CategoryMvpView> categoryMvpViewCategoryPresenter;


    CategoryAdapter categoryAdapter;


    RecyclerView recyclerView;


    public static final String TAG = "Category";




    public static CategoryFragment newInstance() {
        Bundle args = new Bundle();
        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);


        View view = inflater.inflate(R.layout.categories, container, false);
        this.mView = view;
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            try {


                recyclerView = (RecyclerView) mView.findViewById(R.id.list_category);
                component.inject(this);

                categoryMvpViewCategoryPresenter.onAttach(this);
                categoryMvpViewCategoryPresenter.onserverclick();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        RecyclerItemClickSupport.addTo(recyclerView).setOnItemClickListener(new RecyclerItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                openSubCategory(String.valueOf(position + 1));


            }
        });
        return view;
    }

    @Override
    public void openSubCategory(String id) {
        FragmentManager fragmentManager = getBaseActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

//        fragmentManager
//                .beginTransaction().disallowAddToBackStack().add(R.id.containerView, CategoryFragment.newInstance(), CategoryFragment.TAG)
//                .commit();

        SubCategoryFragment fragment3 = new SubCategoryFragment();
        Bundle bun = new Bundle();
        bun.putString("selectedid", id);
        fragment3.setArguments(bun);
        fragmentTransaction.add(R.id.containerView, fragment3, CategoryFragment.TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void bindCategoryList(List<CategoryResponse> categoryResponseList) {
        try {

            categoryAdapter = new CategoryAdapter(categoryResponseList);

            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getBaseActivity(), 2);
            //      RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(categoryAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getDeviceId() {
        return null;
    }

    @Override
    protected void setUp(View view) {

    }
}
