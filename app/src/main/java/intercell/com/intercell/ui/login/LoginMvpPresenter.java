package intercell.com.intercell.ui.login;

import intercell.com.intercell.di.annotation.PerActivity;
import intercell.com.intercell.ui.base.MvpPresenter;

/**
 * Created by Dell on 1/3/2018.
 */
@PerActivity
public interface LoginMvpPresenter<V extends  LoginMvpView> extends MvpPresenter<V>
{

    void onServerLoginClick(String email, String password);

}
