package intercell.com.intercell.ui.signup;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.BaseResponse;
import intercell.com.intercell.datasource.model.SignupResponse;
import intercell.com.intercell.datasource.model.TcResponse;
import intercell.com.intercell.datasource.network.ApiInterface;
import intercell.com.intercell.ui.base.BasePresenter;
import intercell.com.intercell.utils.AppConstants;
import intercell.com.intercell.utils.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 1/10/2018.
 */

public class SignupPresenter<V extends SignupMvpView> extends BasePresenter<V> implements SignupMvpPresenter<V> {


    @Inject
    ApiInterface apiInterface;

    @Inject
    public SignupPresenter() {

    }


    @Override
    public void onServerSignupClick(String firstName, String lastName,
                                    String gender, String role,
                                    String email, String mobile,
                                    String password, String confirmPassword, boolean isTermChecked, String image) {

        if (firstName == null || firstName.isEmpty() || lastName == null || lastName.isEmpty() || gender == null || gender.isEmpty()
                || role == null || role.isEmpty() || email == null || email.isEmpty() || mobile == null || mobile.isEmpty() ||
                password == null || password.isEmpty() || confirmPassword == null || confirmPassword.isEmpty()) {

            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.empty_fileds);
            return;

        }
        if (!CommonUtils.isEmailValid(email)) {
            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.invalid_email);
            return;
        }

        if (!CommonUtils.isValidPassword(password)) {
            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.invalid_password);
            return;
        }
        if (!password.equals(confirmPassword)) {
            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.password_confirm_same);
            return;
        }
        if (!isTermChecked) {
            getMvpView().hideKeyboard();

            getMvpView().onError(R.string.check_tc);
            return;
        }
        getMvpView().showLoading();

        try {


            //Here the json data is add to a hash map with key data
            Map<String, String> params = new HashMap<String, String>();
            params.put("firstname", firstName);
            params.put("lastname", lastName);
            params.put("emailid", email);
            params.put("mobile", mobile);
            params.put("password", password);
            params.put("image", image);
            if (role.equals("I am a mentor")) {
                params.put("usertype", "3");

            } else if (role.contains("Student")) {
                params.put("usertype", "2");
            } else {
                params.put("usertype", "4");
            }

            params.put("gender", gender);
            params.put("os", AppConstants.osType);
            params.put("device_id", getMvpView().getDeviceId());

            apiInterface.getRegister(params).enqueue(new Callback<BaseResponse<SignupResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<SignupResponse>> call, Response<BaseResponse<SignupResponse>> response) {
                    getMvpView().hideLoading();

                    if (response.body().getStatus() == 0) {
                        getMvpView().onError(response.body().getMessage());

                    } else {
                        SignupResponse signupResponse = (SignupResponse) response.body().getResponse().get(0);
                        getMvpView().openOtpAcivity(signupResponse);


                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<SignupResponse>> call, Throwable t) {
                    getMvpView().hideLoading();

                }
            });
        } catch (Exception e) {

        }


    }
}
