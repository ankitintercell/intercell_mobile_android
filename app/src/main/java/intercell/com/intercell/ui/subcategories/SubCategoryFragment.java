package intercell.com.intercell.ui.subcategories;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import intercell.com.intercell.R;
import intercell.com.intercell.datasource.model.SubCategories;
import intercell.com.intercell.datasource.model.SubCategoryResponse;
import intercell.com.intercell.di.component.ActivityComponent;
import intercell.com.intercell.ui.base.BaseFragment;
import intercell.com.intercell.ui.category.CategoryFragment;
import intercell.com.intercell.ui.custom.RecyclerItemClickSupport;
import intercell.com.intercell.ui.mentorlist.MentorListFragment;

/**
 * Created by Dell on 1/24/2018.
 */

public class SubCategoryFragment extends BaseFragment implements SubCategoryMvpView {
    protected View mView;
    @Inject
    SubCategoryPresenter<SubCategoryMvpView> subCategoryMvpViewSubCategoryPresenter;
    public static final String TAG = "SubCategory";

    String selectedId = "";
    SubCategoryAdapter subCategoryAdapter;
    List<SubCategories> subCategoriesList = new ArrayList<>();
    List<SubCategories> selectedSubCategoriesList = new ArrayList<>();
    RecyclerView lst_subcategory;
    Button btn_proceed;
    CheckBox chk_select_all;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        selectedId = getArguments().getString("selectedid");
        View view = inflater.inflate(R.layout.sub_categories, container, false);
        this.mView = view;
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            try {


                lst_subcategory = (RecyclerView) mView.findViewById(R.id.lst_subcategory);
                btn_proceed = (Button) mView.findViewById(R.id.btn_proceed);
                chk_select_all = (CheckBox) mView.findViewById(R.id.chk_select_all);
                component.inject(this);
                subCategoryMvpViewSubCategoryPresenter.onAttach(this);
                subCategoryMvpViewSubCategoryPresenter.onGetSubCategory(selectedId);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        RecyclerItemClickSupport.addTo(lst_subcategory).setOnItemClickListener(new RecyclerItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {


//                String text = "";
//                for (Model model : mModelList) {
//                    if (model.isSelected()) {
//                        text += model.getText();
//                    }
//                }
//                Log.d("TAG","Output : " + text);

            }
        });


        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (SubCategories subCategories : subCategoriesList) {
                    if (subCategories.isSelected()) {

                        selectedSubCategoriesList.add(subCategories);
                    }
                }

                if (selectedSubCategoriesList.size() > 0) {


                    FragmentManager fragmentManager = getBaseActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    MentorListFragment fragment = new MentorListFragment();
                    Bundle bun = new Bundle();
                    bun.putSerializable("selectedIdList", (Serializable) selectedSubCategoriesList);
                    bun.putString("selectCategoryId", selectedId);
                    fragment.setArguments(bun);
                    fragmentTransaction.add(R.id.containerView, fragment, CategoryFragment.TAG);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                } else
                    onError(R.string.select_any_steam);


            }
        });

        chk_select_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    for (SubCategories subCategories : subCategoriesList) {
                        subCategories.setSelected(true);


                    }


                } else {
                    for (SubCategories subCategories : subCategoriesList) {
                        subCategories.setSelected(false);


                    }


                }
                subCategoryAdapter.notifyDataSetChanged();
                lst_subcategory.setAdapter(subCategoryAdapter);
            }
        });
        return view;
    }

    @Override
    public String getDeviceId() {
        return null;
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void bindSubCategoryList(SubCategoryResponse subCategoryResponse) {
        for (int i = 0; i < subCategoryResponse.getServices().size(); i++) {
            SubCategories subCategories = subCategoryResponse.getServices().get(i);
            subCategoriesList.add(subCategories);

        }

        subCategoryAdapter = new SubCategoryAdapter(subCategoriesList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getBaseActivity(), 1);
        lst_subcategory.setLayoutManager(mLayoutManager);
        lst_subcategory.setItemAnimator(new DefaultItemAnimator());
        lst_subcategory.setAdapter(subCategoryAdapter);

    }

    @Override
    public void openMentorList() {

    }
}
