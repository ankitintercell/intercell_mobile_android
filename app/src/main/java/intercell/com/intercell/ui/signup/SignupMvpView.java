package intercell.com.intercell.ui.signup;

import intercell.com.intercell.datasource.model.SignupResponse;
import intercell.com.intercell.ui.base.MvpView;

/**
 * Created by Dell on 1/10/2018.
 */

public interface SignupMvpView extends MvpView {

    void openOtpAcivity(SignupResponse signupResponse);
}
