package intercell.com.intercell.ui.base;

/**
 * Created by Dell on 12/18/2017.
 */

public interface DialogMvpView extends MvpView {
    void dismissDialog(String tag);
}
